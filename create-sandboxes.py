#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) Parallels Software International, Inc. 2013
#
import sys, time, getopt, operator, re, random, ConfigParser
import prlsdkapi
consts = prlsdkapi.prlsdk.consts

# An exception class to use to terminate the program.
class Halt(Exception):
    pass

# IP address
class IPv4Address(object):
    """docstring for IPv4Address"""
    def __init__(self, IPv4_Address_str_or_CIDR):
            super(IPv4Address, self).__init__()

            self.is_Valid = False
            self.is_Valid_Details = ['IP address is not validated yet']
            self.octets = []
            self.IPv4_address_str = ''
            self.mask_CIDR_str = '24' #default
            self.mask_CIDR_int = 0

            self.IPv4_address_int = 0
            self.mask_int = 0

            self.network_address_int = 0
            self.network_broadcast_address_int = 0

            #print IPv4_Address_str_or_CIDR
            self.parse(IPv4_Address_str_or_CIDR)
    
    def get_geteway_IP(self) :
        return IPv4Address(self.ip_int2str(self.network_address_int + 1, self.mask_CIDR_int))

    def get_next_IPv4(self, offset):
        next_IPv4 = self.IPv4_address_int + offset
        return IPv4Address(self.ip_int2str(next_IPv4,self.mask_CIDR_int))

    def parse(self, IPv4_Address_str_or_CIDR):
        try:
            # try to exclude CIDR mask
            self.IPv4_address_str, self.mask_CIDR_str = IPv4_Address_str_or_CIDR.split('/')
        except ValueError:
            # consider IP address without CIDR mask (/24 to be used as default)
            self.IPv4_address_str = IPv4_Address_str_or_CIDR       
        #print self.mask_CIDR_str
        try:
            # get octets from IP address
            self.octets = [int(x) for x in self.IPv4_address_str.split('.')]
            self.mask_CIDR_int = int(self.mask_CIDR_str)
            self.is_Valid = self.mask_CIDR_int < 0 or self.mask_CIDR_int > 31
            # convert IP to int
            self.IPv4_address_int = (self.octets[0] << 24) + (self.octets[1] << 16) + (self.octets[2] << 8) + self.octets[3] 
            # convert network mask from CIDR to int
            self.mask_int = (~0 << (32 - self.mask_CIDR_int))
            # calculate network address int
            self.network_address_int = (self.IPv4_address_int & self.mask_int)
            # calcuate network broadcasts address 
            self.network_broadcast_address_int = (self.network_address_int | ~(self.mask_int))
            #print self.ip_int2strCIDR(self.network_broadcast_address_int, self.mask_CIDR_int)
            # validate IP
            # - octets inside range 0-255
            # - first octet shoduld be not 0 or 127
            # - ip address shoduld be not broadcast
            self.is_Valid = (len([x for x in self.octets if (x < 0 or x > 255)]) == 0) and (self.octets[0] != 0) and (self.octets[0] != 127) #and (self.IPv4_address_int != self.network_address_int) and (self.IPv4_address_int != self.network_broadcast_address_int)
            if not  self.is_Valid :
                self.is_Valid_Details = ["network configuration error : ip address '" + IPv4_Address_str_or_CIDR + "' is in wrong format"]
        except ValueError:
            self.is_Valid_Details = ["network configuration error : ip address '" + IPv4_Address_str_or_CIDR + "' is in wrong format"];
            return
        if self.is_Valid :
            self.is_Valid_Details = []        

    def ip_int2longstr(self, ip_int, mask_int):
        return "{0}.{1}.{2}.{3}/{4}.{5}.{6}.{7}".format( ((ip_int >> 24) & 255), (ip_int >> 16) & 255, (ip_int >> 8) & 255, ip_int & 255, ((mask_int >> 24) & 255), (mask_int >> 16) & 255, (mask_int >> 8) & 255, mask_int & 255) 

    def ip_int2shortstr(self, ip_int):
        return "{0}.{1}.{2}.{3}".format( ((ip_int >> 24) & 255), (ip_int >> 16) & 255, (ip_int >> 8) & 255, ip_int & 255) 

    def ip_int2str(self, ip_int, mask_int):
        return "{0}.{1}.{2}.{3}/{4}".format( ((ip_int >> 24) & 255), (ip_int >> 16) & 255, (ip_int >> 8) & 255, ip_int & 255 , mask_int) 

    def ip_by_long_str(self):
        return self.ip_int2longstr(self.IPv4_address_int, self.mask_int)

    def ip_by_short_str(self):
        return self.ip_int2shortstr(self.IPv4_address_int)

    def __repr__(self):
        return self.ip_int2str(self.IPv4_address_int, self.mask_CIDR_int)
# IP pool
class IPv4Pool(object):
    """docstring for IPv4Pool"""
    def __init__(self, start_IPv4Address, end_IPv4Address):
        super(IPv4Pool, self).__init__()
        self.start_IPv4Address = start_IPv4Address
        self.end_IPv4Address = end_IPv4Address
        self.is_Valid_Details = ["IP pool is not configured yet"]
        self.capacity = 0;
        try:
            # suppose all ok
            self.is_Valid = True 
            self.is_Valid_Details = [] 

            # check and validade IP Pool
            # - start <= end
            # - start and end in the same network 
            # - start and end are both valid ;)
            self.is_Valid = (start_IPv4Address.IPv4_address_int <= end_IPv4Address.IPv4_address_int) and start_IPv4Address.is_Valid and end_IPv4Address.is_Valid and start_IPv4Address.network_broadcast_address_int == end_IPv4Address.network_broadcast_address_int
            if not self.is_Valid :           
                self.is_Valid_Details.extend = ["ip pool configuration error : wrong network mask or not valid ip addresses"]

            # calculate capacity of Pool
            self.capacity = end_IPv4Address.IPv4_address_int - start_IPv4Address.IPv4_address_int + 1
            if self.capacity < 1:
                self.is_Valid = False
                self.is_Valid_Details.extend(["ip pool configuration error : ip pool has no capacity"])

        except Exception, e:
            self.is_Valid = False
            self.is_Valid_Details.extend(["ip pool configuration error : IP pool is not valid" + str(e)])
            return
        # check if IP's are valid
        if not self.start_IPv4Address.is_Valid :
            self.is_Valid = False
            self.is_Valid_Details.extend(self.start_IPv4Address.is_Valid_Details)
        if not self.end_IPv4Address.is_Valid :
            self.is_Valid = False
            self.is_Valid_Details.extend(self.end_IPv4Address.is_Valid_Details)

    def get_ip_by_index(self, offset):
        if self.is_Valid :
            return self.start_IPv4Address.get_next_IPv4(offset)
        return

    def get_geteway_IP(self):
        if self.is_Valid :
            return self.start_IPv4Address.get_geteway_IP()
        return

# Virtual Environment config
class VEConfiguration(object):
    """docstring for VEConfiguration"""
    VE_TYPE_LINUX_JUMPER = 0;
    VE_TYPE_LIN_FE = 1;
    VE_TYPE_LIN_BE = 2;

    def __init__(self, ve_config):
        super(VEConfiguration, self).__init__()

        # @ve-config - string like [vm_id]:[vm_type_id 1-frontend;2]:[vm_template_version]
        #
        #
        self.ve_id = -1
        self.ve_template_version = -1
        self.ve_type = -1 
        self.is_Valid = False
        self.is_Valid_Details = ["VE Configuration is not valided yet"]
        #self.additional_ips = new dict()
        try:
            self.ve_id, self.ve_type, self.ve_template_version = [int(x) for x in ve_config.split(':')]
        except ValueError:
            self.is_Valid_Details = ["ves-config value error : could not convert ve_id or ve_type or ve_template_version to an integer in '" + ve_config + "'"]
            return 
        self.is_Valid = True

    def __repr__(self):
        return '{0:02d}:{1:02d}:{1:03d}'.format(self.ve_id, self.ve_type, self.ve_template_version)
# Lab Environment config
class LabConfiguration(object):
    def __init__(self):
        super(LabConfiguration, self).__init__()
        
        self.sandboxes_list = []
        self.providers_zone = ""
        self.dns_layout = {}
        self.ip_layout = {}

        self.is_Valid = False
        self.is_Valid_Details = ['Config is not readed yet']
        # Section[Lab Environment Network Configuration]
        # Start IP address of Jumper's IP pool
        # jumpers-ip-pool-start = 10.30.122.121/24
        self.jumpers_ip_pool_start = IPv4Address("0.0.0.0")
        # End IP adddress of Jumper's IP pool
        # jumpers-ip-pool-end = 10.30.122.141/24
        self.jumpers_ip_pool_end = IPv4Address("0.0.0.0")
        # Resolvers to be used by Jumper
        # jumpers-nameservers = 10.30.0.27,10.30.0.28
        self.jumpers_nameservers = ''
        # Secton[Lab Environment Configuration]
        # SKU of training course lab envronment 
        self.course_SKU = ""
        # Version of templates to be used for creating lab envronment 
        self.version = ""
        # List of sandboxes to be created (1..N) 
        # example: 1; 2; 3; 4; 5; 6; 7 
        self.sandboxes = []
        # Section [Sandbox Config]
        # path on PCS server for VE's
        # ves-path = /vmdata
        self.ves_path = ''
        # List of Virtual Machines to be created inside sandbox
        # [ve number]:[ve_type 0:1:2]:[version of template]; 
        self.cfg = object
        self.ves_config = dict()
        # Automatically start all VM's
        self.autostart_ves = False
        # Remove existing templates before
        self.remove_existing_ves = False

        self.frontnet_ip_pool = IPv4Pool(IPv4Address('10.111.11.10/24'),IPv4Address('10.111.11.254/24'))
        self.backnet_ip_pool = IPv4Pool(IPv4Address('10.111.22.10/24'),IPv4Address('10.111.22.254/24'))

    def validate(self):
        # check if IP Pool is validated
        if not self.jumpers_ip_pool.is_Valid :
            self.is_Valid = False
            self.is_Valid_Details.extend(self.jumpers_ip_pool.is_Valid_Details)

        # check if there is IP address available in the jumper's for each sandbox
        for x in [y for y in self.sandboxes if y > self.jumpers_ip_pool.capacity] : 
            self.is_Valid = False
            self.is_Valid_Details.extend(["network or sandbox configuration error : There is no IP address into the IP pool for Sandbox #" + str(x)])

        # Check if all VE configurations are ok 
        for x in [y for y in self.ves_config.values() if not y.is_Valid] :
            self.is_Valid = False
            self.is_Valid_Details.extend ([y for y in x.is_Valid_Details])

    def read(self, path):
        config = ConfigParser.ConfigParser()
        try:
            config.read('/root/sandbox/hwnode.cfg')
            # Parse per-server configuration
            self.jumpers_ip_pool = IPv4Pool( IPv4Address(config.get('Lab Environment Network Configuration', 'jumpers-ip-pool-start')), \
                                 IPv4Address(config.get('Lab Environment Network Configuration', 'jumpers-ip-pool-end')))
            self.jumpers_nameservers = config.get('Lab Environment Network Configuration', 'jumpers-nameservers')
            config.read(path)
            # suppose all are ok
            self.is_Valid = True
            self.is_Valid_Details = []
            # Secton [Lab Environment Configuration]
            self.course_SKU = config.get('Lab Environment Configuration', 'course-SKU')
            self.providers_zone = config.get('Lab Environment Configuration', 'providers_zone')
            self.version = config.get('Lab Environment Configuration', 'version')
            # Get list of sandboxes either from CLI options or from config
            if self.sandboxes_list.__len__() > 0:
                self.sandboxes = [int(x) for x in self.sandboxes_list.split(',')]
            else:
                self.sandboxes = [int(x) for x in config.get('Lab Environment Configuration', 'sandboxes').split(';')]
            # Section [Hosts Layout]
            for l in config.get('Hosts Layout','dns-layout').split(';'):
                for k,v in [ l.split(':') ]:
                    self.dns_layout[int(k)] = v
            for l in config.get('Hosts Layout','ip-layout').split(';'):
                for k,v in [ l.split(':') ]:
                    self.ip_layout[int(k)] = v
            # Section [Sandbox Config]            
            for x in config.get('Sandbox Config', 'ves-config').split(';'):
                self.cfg = VEConfiguration(x)
                self.ves_config[self.cfg.ve_id] = self.cfg
            # Automatically start all VM's
            self.autostart_ves = config.get('Sandbox Config', 'autostart-ves') == "yes"
            # Remove existing templates before
            self.remove_existing_ves = config.get('Sandbox Config', 'remove-existing-ves') == "yes"
            self.ves_path = config.get('Sandbox Config', 'ves-path')

            self.validate()
        except ConfigParser.Error, e:
            self.is_Valid_Details = ["Lab configuration error : Error in reading configuration file: %s" % e]
            self.is_Valid = False
            return
        except:
            self.is_Valid_Details = ["Unexpected error:", sys.exc_info()[0]]
            self.is_Valid = False
            raise
    def get_ve_config(self, ve_id):
        if self.is_Valid:
            return self.ves_config[ve_id]
        return ''
    def get_ve_name(self, sandbox_id, ve_id):
        if self.is_Valid:
            #SANDBOX_ID : VE_ID 
            if ve_id != 0:
                return "ve-{0:02d}-{1:02d}".format(sandbox_id, ve_id)
            else:
                return "jumper-{0:02d}".format(sandbox_id)
        return ''
    def get_ve_virtual_net(self, sandbox_id, ve_id):
        if self.is_Valid:
            #SANDBOX_ID : VE_ID 
            return "sandbox-{0:02d}-VLAN".format(sandbox_id)
        return ''
    def get_ve_hostname(self, ve_id):
        if self.is_Valid:
            tmp_hostname = "ve_{0:02d}.{1}".format(ve_id, self.providers_zone)
            try : 
                tmp_hostname = "{0}.{1}".format(self.dns_layout[ve_id], self.providers_zone)
            except KeyError:
                pass
            return tmp_hostname
        return ''
    def get_ve_template_name(self, ve_id):
        if self.is_Valid:
            tmp_ve_cfg = self.ves_config[ve_id]
            if ve_id != 0:
               #COURSE_SKU : COURSE_VERSION : VE_TEMPLATE_VERSION : VE_ID 
                return "Template_{0}_v{1}_t{2:02d}_ve{3:02d}".format(self.course_SKU, self.version, tmp_ve_cfg.ve_template_version, ve_id);
            else:        
                return "Template_{0}_v{1}_t{2:02d}_jumper".format(self.course_SKU, self.version, tmp_ve_cfg.ve_template_version);
        return ''
    def get_ip_by_layout(self,ip_pool,ve_id):
        ip = ip_pool.get_ip_by_index(ve_id)
        if self.is_Valid:
            try :
                ip.octets[3] = self.ip_layout[ve_id]
                ip.parse("{0}.{1}.{2}.{3}".format(ip.octets[0],ip.octets[1],ip.octets[2],ip.octets[3]))
            except KeyError:
                pass
        return ip

# Lab Environment config
class LabBuilder(object):
    def __init__(self, lab_cfg):
        super(LabBuilder, self).__init__()
        self.lab_config = lab_cfg

        # Initialize the library for Parallels Server
        prlsdkapi.init_server_sdk()
        self.pcs_server = prlsdkapi.Server()

        # Initialize cache for vm_list and container_list on the server
        self.pcs_list_of_vms = ''
        self.pcs_list_of_cts = ''

    def setup_net(self, ve_config, net_adapter_index, ip_addr, gateway, ns_servers, virtual_net):
        if self.lab_config.is_Valid:
            self.add_virtual_network(virtual_net)
            ve_net_adapter = ve_config.get_net_adapter(net_adapter_index)
            print "eth{0} : {1}; gw: {2}; ns: {3}; vlan: {4}".format(net_adapter_index,
                                                                     ip_addr.ip_by_long_str(),
                                                                     gateway, 
                                                                     ns_servers,
                                                                     virtual_net)
            net_addr_list = prlsdkapi.StringList()
            net_addr_list.add_item(ip_addr.ip_by_long_str())
            ve_net_adapter.set_net_addresses(net_addr_list) #int
            if gateway != '':
                ve_net_adapter.set_default_gateway(gateway.ip_by_short_str()) #str

            ns_serv_list = prlsdkapi.StringList()
            ns_serv_arr = ns_servers.split(',')
            for ns_addr in ns_serv_arr:
                ns_serv_list.add_item(ns_addr)

            ve_net_adapter.set_dns_servers(ns_serv_list) #[] of ints
            if virtual_net != '':
               ve_net_adapter.set_virtual_network_id(virtual_net)

    def add_virtual_network(self, virtual_net):
        if virtual_net == '':
            return
        if self.lab_config.is_Valid:
            virtual_networks = self.pcs_server.get_virtual_network_list(0).wait()
            flag_add = True
            for vnet_id in range(virtual_networks.get_params_count()):
                if virtual_net == virtual_networks.get_param_by_index(vnet_id).get_network_id():
                    print "Virtual network '{0}' already exists".format(virtual_net)
                    flag_add = False
            if flag_add :
                print "Adding Virtual network '{0}'".format(virtual_net)
                new_vnet = prlsdkapi.VirtualNet()
                new_vnet.set_network_type(1)
                new_vnet.set_network_id(virtual_net)
                new_vnet.set_description("Host-only network for sandbox")
                new_vnet.set_adapter_name("Host-only network for '{0}'".format(virtual_net))
                new_vnet.set_adapter_enabled(True)
                new_vnet.set_dhcpserver_enabled(False)
                self.pcs_server.add_virtual_network(new_vnet, 0).wait()
                print "Done"

    def build_sandbox_ve(self, sandbox_id, ve_id):

        if self.lab_config.is_Valid :
            print_log("Building ve #" + str(ve_id))      
            print "----------------------------------------------"
            ve_config = self.lab_config.get_ve_config(ve_id)
            ve_template_name = self.lab_config.get_ve_template_name(ve_id)

            # Checking template 

            template_ve = self.search_vm(ve_template_name);
            if not isinstance(template_ve, prlsdkapi.Vm):
                print "VE template {0} does not exists. VE is not created.".format(ve_template_name)
                print "----------------------------------------------"
                return
            else:
                print "VE template {0} is OK".format(ve_template_name)

            # Check if VE already exists
            new_ve_name = self.lab_config.get_ve_name(sandbox_id, ve_id)
            new_ve = self.search_vm(new_ve_name)
            if isinstance(new_ve, prlsdkapi.Vm):
                print "VE '{0}' already exists.".format(new_ve_name)
                if self.lab_config.remove_existing_ves:
                    print "Autoremoving '{0}'".format(new_ve_name)
                    # Removing VE
                     # Unregister the machine and delete its files from the hard drive.
                    try:
                        new_ve.delete().wait()
                        print_log(" Done")
                    except prlsdkapi.PrlSDKError, e:
                        print "Error: %s" % e
                        print "----------------------------------------------"
                        return
                else:
                    print "----------------------------------------------"
                    return

            # Creating VE
            print "Creating VE#" + str(ve_id) + " '" + self.lab_config.get_ve_name(sandbox_id, ve_id) + "' based on template '" + ve_template_name + "'..." 

            try:
                # Second parameter - create a new machine in the
                # default directory.
                # Third parameter - create a virtual machine (not a template).
                #template_ve.clone(self.lab_config.get_ve_name(sandbox_id, ve_id), self.lab_config.ves_path, False).wait()
                template_ve.clone_ex(self.lab_config.get_ve_name(sandbox_id, ve_id), self.lab_config.ves_path, 8192).wait()
                print "Done"
            except prlsdkapi.PrlSDKError, e:
                print "Error: %s" % e
                print "----------------------------------------------"
                return

            new_ve = self.search_vm_direct(new_ve_name)
            if isinstance(new_ve, prlsdkapi.Vm):

                # Apply settings to VE
                print "Applying VE#" + str(ve_id) + " '" + self.lab_config.get_ve_name(sandbox_id, ve_id) + " settings." 

                # Begin the virtual machine editing operation.
                try:
                    new_ve.begin_edit().wait()
                except prlsdkapi.PrlSDKError, e:
                    print "Error: %s" % e
                    return
                # Obtain the VmConfig object containing the virtual machine
                # configuration information.
                new_ve_config = new_ve.get_config()

                # VE name and description
                print "VE name : {0}".format(self.lab_config.get_ve_name(sandbox_id, ve_id))
                new_ve_config.set_name(self.lab_config.get_ve_name(sandbox_id, ve_id))
                new_ve_config.set_description("Sandbox#{0} : {1}".format(sandbox_id, new_ve_config.get_description()) )
                print "Hostname : {0}".format(self.lab_config.get_ve_hostname(ve_id))
                new_ve_config.set_hostname(self.lab_config.get_ve_hostname(ve_id))

                # VE name and description
                if (ve_config.ve_type == 0):
                    # Public net
                    eth0_IP = self.lab_config.jumpers_ip_pool.get_ip_by_index(sandbox_id - 1)
                    self.setup_net( new_ve_config,
                                    0,
                                    eth0_IP,
                                    '',
                                    self.lab_config.jumpers_nameservers,
                                    '')
                    eth1_IP = self.lab_config.frontnet_ip_pool.get_geteway_IP()
                    self.setup_net( new_ve_config,
                                    1,
                                    eth1_IP,
                                    '',
                                    '',
                                    self.lab_config.get_ve_virtual_net(sandbox_id, ve_id))

                    eth2_IP = self.lab_config.backnet_ip_pool.get_geteway_IP()
                    self.setup_net( new_ve_config,
                                    2,
                                    eth2_IP,
                                    '',
                                    '',
                                    self.lab_config.get_ve_virtual_net(sandbox_id, ve_id))

                elif (ve_config.ve_type == 1):
                    eth1_IP = self.lab_config.get_ip_by_layout(self.lab_config.frontnet_ip_pool,ve_id)
                    #eth1_IP = self.lab_config.frontnet_ip_pool.get_ip_by_index(ve_id)  
                    self.setup_net( new_ve_config, 0,
                                    eth1_IP, eth1_IP.get_geteway_IP(), '10.111.11.1', self.lab_config.get_ve_virtual_net(sandbox_id, ve_id))

                    eth2_IP = self.lab_config.get_ip_by_layout(self.lab_config.backnet_ip_pool,ve_id)
                    #eth2_IP = self.lab_config.backnet_ip_pool.get_ip_by_index(ve_id)         
                    self.setup_net( new_ve_config, 1,
                                    eth2_IP, eth2_IP.get_geteway_IP(), '10.111.22.1', self.lab_config.get_ve_virtual_net(sandbox_id, ve_id))

                elif (ve_config.ve_type == 2):
                    eth2_IP = self.lab_config.get_ip_by_layout(self.lab_config.backnet_ip_pool,ve_id)
                    #eth2_IP = self.lab_config.backnet_ip_pool.get_ip_by_index(ve_id)
                    self.setup_net( new_ve_config, 0,
                                    eth2_IP, eth2_IP.get_geteway_IP(), '10.111.22.1', self.lab_config.get_ve_virtual_net(sandbox_id, ve_id))

                elif (ve_config.ve_type == 3):
                    eth1_IP = self.lab_config.get_ip_by_layout(self.lab_config.frontnet_ip_pool,ve_id)
                    self.setup_net( new_ve_config, 0,
                                    eth1_IP, eth1_IP.get_geteway_IP(), '10.111.11.1', self.lab_config.get_ve_virtual_net(sandbox_id, ve_id))

                # Commit the changes.
                try:
                    new_ve.commit().wait()
                except prlsdkapi.PrlSDKError, e:
                    print "Error: %s" % e
                    return

            print "----------------------------------------------"
    def build_sandbox(self, sandbox_id):

        if self.lab_config.is_Valid and sandbox_id in self.lab_config.sandboxes:
            # building sandbox 
            print_log("Building sandbox #" + str(sandbox_id))      
            print "----------------------------------------------"

            # building Virtual Environments
            for ve_id in self.lab_config.ves_config:
                self.build_sandbox_ve(sandbox_id, ve_id)

            #check if templates also have containers


        return

    # Build lab config
    def build(self):

        if self.lab_config.is_Valid:

            # connecting to PCS
            print_log("Connecting to PCS (localhost)")        
            print "----------------------------------------------"

            self.login_server(self.pcs_server, "localhost", "", "", consts.PSL_NORMAL_SECURITY);

            # building sandboxes
            for sandbox_id in self.lab_config.sandboxes:
                self.build_sandbox(sandbox_id)


            print "----------------------------------------------"
        return



    #
    # Obtain a Server object for the PCS server.
    #                  
    def login_server(self, server, host, user, password, security_level):
        # Local or remote login?
        if host=="localhost":
            try:
                # The call returns a prlsdkapi.Result object on success.
                result = server.login_local('', 0, security_level).wait()
            except prlsdkapi.PrlSDKError, e:
                print "Login error: %s" % e
                raise Halt
        else: 
            try:
                # The call returns a prlsdkapi.Result object on success.
                result = server.login(host, user, password, '', 0, 0, security_level).wait()
            except prlsdkapi.PrlSDKError, e:
                print "Login error: %s" % e
                print "Error code: " + str(e.error_code)
                raise Halt
        # Obtain a LoginResponse object contained in the Result object.
        # LoginResponse contains the results of the login operation.
        login_response = result.get_param()
        # Get the Parallels virtualization product version number.
        product_version = login_response.get_product_version()
        # Get the host operating system version.
        host_os_version = login_response.get_host_os_version()
        # Get the host UUID.
        host_uuid = login_response.get_server_uuid()
        print""
        print "Login successful"
        print""
        print "Parallels product version: " + product_version
        print "Host OS verions:           " + host_os_version
        print "Host UUID:                 " + host_uuid
        print ""
        return result
    #
    # Obtain a Vm object for the virtual machine specified by its name.
    # @param vm_to_find: Exact name of the virtual machine to find.
    #                  
    def search_vm_direct(self, vm_to_find):
        self.pcs_list_of_vms = ''
        return self.search_vm(vm_to_find)         

    def search_vm(self, vm_to_find):
        if vm_to_find == '':
            return
        try:
            if self.pcs_list_of_vms == '':
               self.pcs_list_of_vms = self.pcs_server.get_vm_list().wait()
        except prlsdkapi.PrlSDKError, e:
            print "Error in searching vm '" + vm_to_find + "': %s" % e
            return
        for i in range(self.pcs_list_of_vms.get_params_count()):
            vm = self.pcs_list_of_vms.get_param_by_index(i)
            vm_name = vm.get_name()
            if vm_name == vm_to_find:
                return vm
        return ''

    def vm_edit(vm):
        # Begin the virtual machine editing operation.
        try:
            vm.begin_edit().wait()
        except prlsdkapi.PrlSDKError, e:
            print "Error: %s" % e
            return
        # Obtain the VmConfig object containing the virtual machine
        # configuration information.
        vm_config = vm.get_config()
        vm.set_name(vm.get_name() + "_modified")
        vm.set_ram_size(256)
        vm.set_description("SDK Test Machine")
        # Modify boot device priority using the following order:.
        # CD > HDD > Network > FDD.
        # Remove all other devices from the boot priority list (if any).
        count = vm_config.get_boot_dev_count()
        for i in range(count):
            # Obtain an instance of the prlsdkapi.BootDevice class
            # containing the boot device information.
            boot_dev = vm_config.get_boot_dev(i)
            # Enable the device.
            boot_dev.set_in_use(True)
            # Set the device sequence index.
            dev_type = boot_dev.get_type()
        if dev_type == consts.PDE_OPTICAL_DISK:
            boot_dev.set_sequence_index(0)
        elif dev_type == consts.PDE_HARD_DISK:
            boot_dev.set_sequence_index(1)
        elif dev_type == consts.PDE_GENERIC_NETWORK_ADAPTER:
           boot_dev.set_sequence_index(2)
        elif dev_type == consts.PDE_FLOPPY_DISK:
            boot_dev.set_sequence_index(3)
        else:
            boot_dev.remove()
        # Commit the changes.
        try:
            vm.commit().wait()
        except prlsdkapi.PrlSDKError, e:
            print "Error: %s" % e
            return

def print_log (log_string):
    localtime = time.asctime( time.localtime(time.time()) )
    print "[{0}] : {1}".format(localtime, log_string)

def get_options():

    global config_path
    global sandboxes_list
    config_path = ''
    sandboxes_list = ''
    
    try:
        opts, args = getopt.getopt(cl_argv,"hcs:",["config=","sandboxes="])
    except getopt.GetoptError:
        print 'Failed to get options: created-sandboxes.py -c <config file name> [-s <semicolumn-separated list of sandboxes>]'
        sys.exit(2)
    for opt, arg in opts:    
        if opt == '-h':
            print 'Printing help: created-sandboxes.py -c <config file name> [-s <semicolumn-separated list of sandboxes>]'
            sys.exit()
        elif opt in ("-c", "--config"):
            config_path = arg
        elif opt in ("-s","--sandboxes"):
            sandboxes_list = arg

    if config_path == '' :
        print 'Empty config path: created-sandboxes.py -c <config file name> [-s <semicolumn-separated list of sandboxes>]'
        sys.exit()


###################################################################################
def main(argv):

    global cl_argv
    cl_argv = argv
    # get options from command line
    get_options()

    #read and validate config file
    lab_config = LabConfiguration()
    print "Reading lab configuration from file..."
    print "----------------------------------------------"
    if len(sandboxes_list) > 0:
        lab_config.sandboxes_list = sandboxes_list
    lab_config.read(config_path)

    if not lab_config.is_Valid :
        print "Lab configuration is not valid, check details :"
        for x in lab_config.is_Valid_Details :
            print "- " + x
        print "----------------------------------------------"
        print "No changes applyed to the server"        
        print "----------------------------------------------"
        sys.exit(2)
    else :
        print "Lab configuration is OK"        
        print "----------------------------------------------"


    # Build Lab

    lab_builder = LabBuilder(lab_config)
    lab_builder.build()


    sys.exit()
    try : 
        # Initialize the library for Parallels Server.
        prlsdkapi.init_server_sdk()
        # Create a Server object and log in to a remote Parallels Server.
        server = prlsdkapi.Server()
        login_server(server, "localhost", "", "", consts.PSL_NORMAL_SECURITY);
    except :
        pass
    # Initialize the library for Parallels Desktop.
    # prlsdkapi.init_desktop_sdk()
    # Initialize the library for Parallels Desktop for Windows and Linux.
    # prlsdkapi.init_desktop_wl_sdk()
    # Initialize the library for Parallels Workstation.
    # prlsdkapi.init_workstation_sdk()
    # Initialize the library for Parallels Player.
    # prlsdkapi.init_player_sdk()
    # Create a Server object and log in to a local
    # Parallels Desktop/Workstation/Desktop for Windows and Linux/Player.
    #
    # server = prlsdkapi.Server()
    # login_server(server, "localhost", "", "", consts.PSL_NORMAL_SECURITY);
    # Log off and deinitialize the library.

    print "Searching for VM '" + vm_name + "'..."
    vm = search_vm(server, vm_name)
    if isinstance(vm, prlsdkapi.Vm):
        print "Found virtual machine " + vm.get_name()
        vm_config = vm.get_config()

        if vm_config.is_template() :
            print "It's template"
        else:
            print "It's Virtual Machine"     
    else:
        print "Virtual machine " + vm_name + "is not found"


    server.logoff()
    prlsdkapi.deinit_sdk()

if __name__ == "__main__":
    try:
        sys.exit(main(sys.argv[1:]))
    except Halt:
        pass
