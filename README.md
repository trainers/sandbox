# Welcome to Parallels Sandbox Project #

Here you may find scripts for automated Training Sandbox Management.

For running Parallels Sandboxes you will need four components:

1. [This set of scripts](http://prlsandbox.com/sandbox.tbz).

2. One or more hardware node with Parallels Cloud Server 6.x installed.

3. IP pool for VNC access to _jumpers_.

4. One or more sandbox _templates_. Template catalog is available [here](http://templates.prlsandbox.com/stable).

## Terminology ##

**Sandbox** - isolated training environment, created by cloning several _template_ virtual machines and configured in accordance with specific _config_ file. Also referred to as _sandbox instance_.

**Template** - pre-deployed virtual machine with all required software installed and network configured.

**Config** - text file used for creating and configuring _sandbox instances_ out of _templates_.

**Jumper** - virtual machine which works as the gateway for every _sandbox instance_ and could be accessed from the external network via VNC.


## Quick Start (PCS Professional Sandbox)##

1. Clone scripts to your hardware node:

	```
	git clone https://bitbucket.org/trainers/sandbox.git
	```

2. Generate the `hwnode.cfg` file, add the following values there:

    * **jumpers-ip-pool-start** - start ip for jumpers assignment
    * **jumpers-ip-pool-end** - end ip for jumper assignment
    * **jumpers-nameservers** - nameservers for jumper. 

3. Get the sandbox template:

	```
	[yourserver]# sandbox template sync PA6_UNFIEID hw-39.prlsandbox.com
	```

	**NOTE**: It's size is several gigabytes, so it may take significant time.

	**NOTE**: See the list of all templates available with `sandbox template list-catalog` command.

4. Create sandboxes:

	```
	./sandbox instance csi 1,2 configs/pba_adv.cfg
	```

5. List the available instances:

	```
	./sandbox instance list
	```

## Structure ##

Main wrapper script:

```
./sandbox
```

Sandbox configs:

```
./configs/*
```

Initialization scripts:

```
./init_scripts/*
```

Samples for various scripts and configs:

```
./samples/*
```

## Operations ##

1. Create sandbox(es):

	```
	./sandbox instance create # CONFIG REQUESTOR FROM_DATE DUE_DATE
	```

	Example:

	In order to create sandboxes 1,3,7 pased on _pcs\_pro.cfg_ config:

	```
	./sandbox instance create 1,2,3 configs/pcs_pro.cfg msudyin 2014-01-01 2014-03-01
	```

2. Start sandbox(es):

	```
	./sandbox instance start 1,2,3
	```

3. Initialize sandbox(es):

	```
	./sandbox instance init 1,2,3
	```

4. Do it alltogether:

	```
	./sandbox instance csi 1,2,3 configs/pcs_pro.cfg msudyin 2014-01-01 2014-03-01
	```

5. Rebuild sandbox(es):

	```
	./sandbox instance rebuild 1,2,3
	```

	**NOTE**: This operation stops the specified sandbox instances, reads the fingerpring files in `/vz/sandbox/.*.fingerprint` and re-runs the `sandbox instance csi...`.

6. List available sandbox(es):

	```
	./sandbox instance list
	```

	In order to list only jumper access information for sandbox instances without checking virtual machine states use `--no-ves` specifier:

	```
	./sandbox instance list --no-ves
	```

For more commands description run `sandbox instance help`.

7. List templates available in web catalog:

	```
	./sandbox template list-catalog
	```

8. Fetch any of templates available in web catalog:

	```
	./sandbox template fetch TEMPLATE_NAME
	```