#!/bin/bash

# Script for initial Sandbox deployment

# Check that we run on a Parallels Cloud Server
VERSION=`prlsrvctl info 2>&1 | awk  '/^Version/{print $2}'`
if [ "$VERSION" != "Server" ];then
	echo -e "ERROR: The software is intended to run on Parallels Cloud Server."
	echo -e "Exiting..."
	exit 1
fi

# Create /vz/sandbox

mkdir -p /vz/sandbox

# Generate hwnode.cfg
JUMPER_IP_START=$1
JUMPER_IP_END=$2
NAMESERVERS=$3


while [ ${#JUMPER_IP_START} -lt 1 ]; do
	echo -e "\nPlease enter the jumper IP addresses pool start IP with the subnet mask."
	echo -e "Example: 192.168.55.11/24."
	echo -n "JUMPER_IP_START:"
	read JUMPER_IP_START
done

while [ ${#JUMPER_IP_END} -lt 1 ]; do
	echo -e "\nPlease enter the jumper IP addresses pool end IP with the subnet mask."
	echo -e "Example: 192.168.55.99/24."
	echo -n "JUMPER_IP_END:"
	read JUMPER_IP_END
done

while [ ${#NAMESERVERS} -lt 1 ]; do
	echo -e "\nPlease enter comma-separated list of name servers for resolving Internet domain names."
	echo -e "Example: 8.8.8.8,8.8.4.4,192.168.55.1"
	echo -n "nameservers:"
	read NAMESERVERS
done

echo "Generating 'hwnode.cfg'"
echo "---------------------
[Lab Environment Network Configuration]

# Start IP address of Jumper's IP pool
jumpers-ip-pool-start = $JUMPER_IP_START

# End IP adddress of Jumper's IP pool
jumpers-ip-pool-end = $JUMPER_IP_END

# Resolvers to be used by Jumper
jumpers-nameservers = 10.111.11.1,$NAMESERVERS
---------------------"

echo "[Lab Environment Network Configuration]

# Start IP address of Jumper's IP pool
jumpers-ip-pool-start = $JUMPER_IP_START

# End IP adddress of Jumper's IP pool
jumpers-ip-pool-end = $JUMPER_IP_END

# Resolvers to be used by Jumper
jumpers-nameservers = 10.111.11.1,$NAMESERVERS" > hwnode.cfg

# Show README
echo -e "Congratulations! You've successfully configured Parallels Sandbox Environment."
echo -e "Please refer to README.md for information on script usage"